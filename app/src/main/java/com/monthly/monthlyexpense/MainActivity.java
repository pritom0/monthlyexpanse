package com.monthly.monthlyexpense;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



import com.monthly.monthlyexpense.mDB.DBAdapter;
import com.monthly.monthlyexpense.mModel.Spacecraft;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.TableDataClickListener;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class MainActivity extends AppCompatActivity {

    EditText dateEditText,launchEditText,dinnerEditText;
    Button saveBtn;
    TableView<String[]> tb;
    TableHelper tableHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        tableHelper = new TableHelper(this);
        tb =(TableView<String[]>) findViewById(R.id.tableView);
        tb.setColumnCount(3);
        tb.setHeaderBackgroundColor(Color.parseColor("#2ecc71"));
        tb.setHeaderAdapter(new SimpleTableHeaderAdapter(this,tableHelper.getSpaceProbeHeaders()));
        tb.setDataAdapter(new SimpleTableDataAdapter(this,tableHelper.getSpaceProbes()));

        tb.addDataClickListener(new TableDataClickListener<String[]>() {
            @Override
            public void onDataClicked(int rowIndex, String[] clickedData) {
                Toast.makeText(MainActivity.this,((String[])clickedData)[1],Toast.LENGTH_SHORT).show();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             displayDialog();
            }
        });
    }


    private void displayDialog()
    {
        Dialog d= new Dialog(this);
        d.setTitle("SQLITE DATA");
        d.setContentView(R.layout.dialog_layout);

        dateEditText = (EditText) d.findViewById(R.id.dateEditTxt);
        launchEditText=(EditText) d.findViewById(R.id.launchEditTxt);
        dinnerEditText=(EditText)d.findViewById(R.id.dinnerEditTxt);

        saveBtn = (Button) d.findViewById(R.id.saveBtn);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Spacecraft s = new Spacecraft();
                s.setDate(dateEditText.getText().toString());
                s.setLaunch(launchEditText.getText().toString());
                s.setDinner(dinnerEditText.getText().toString());

                if (new DBAdapter(MainActivity.this).saveSpacecraft(s)){
                    dateEditText.setText("");
                    launchEditText.setText("");
                    dinnerEditText.setText("");

                    tb.setDataAdapter(new SimpleTableDataAdapter(MainActivity.this,tableHelper.getSpaceProbes()) );
                } else {
                    Toast.makeText(MainActivity.this,"Not Saved",Toast.LENGTH_SHORT).show();
                }

            }
        });

        d.show();
    }


}
