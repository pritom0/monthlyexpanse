package com.monthly.monthlyexpense;

import android.content.Context;

import com.monthly.monthlyexpense.mDB.DBAdapter;
import com.monthly.monthlyexpense.mModel.Spacecraft;

import java.util.ArrayList;

public class TableHelper {

    Context c;
    private String[] spaceProbeHeaders = {"Date","Launch","Dinner"};
    private String[][] spaceProbes;

    public TableHelper(Context c){
        this.c =c;
    }

    public String[] getSpaceProbeHeaders() {
        return spaceProbeHeaders;
    }

    public String[][] getSpaceProbes() {

        ArrayList<Spacecraft> spacecrafts=new DBAdapter(c).retrieveSpacecrafts();
        Spacecraft s;

        spaceProbes = new String[spacecrafts.size()][3];

        for(int i=0;i<spacecrafts.size();i++){

            s=spacecrafts.get(i);

            spaceProbes[i][0]=s.getDate();
            spaceProbes[i][1]=s.getLaunch();
            spaceProbes[i][2]=s.getDinner();
        }
        return spaceProbes;
    }
}
