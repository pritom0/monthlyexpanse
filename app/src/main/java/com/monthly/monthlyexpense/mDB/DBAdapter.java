package com.monthly.monthlyexpense.mDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.monthly.monthlyexpense.mModel.Spacecraft;

import java.util.ArrayList;

public class DBAdapter {


    Context c;
    SQLiteDatabase db;
    DBHelper helper;

    public DBAdapter(Context c){
        this.c = c;
        helper = new DBHelper(c);
    }

    public boolean saveSpacecraft(Spacecraft spacecraft){
        try{
            db = helper.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(Constants.DATE,spacecraft.getDate());
            cv.put(Constants.LAUNCH,spacecraft.getLaunch());
            cv.put(Constants.DINNER,spacecraft.getDinner());

            long result = db.insert(Constants.TB_NAME,Constants.ROW_ID,cv);

            if (result>0){

                return true;
            }


            } catch (SQLException e){
            e.printStackTrace();
        } finally {
            helper.close();
        }

        return false;
    }


    public ArrayList<Spacecraft> retrieveSpacecrafts()
    {
        ArrayList<Spacecraft> spacecrafts = new ArrayList<>();

        String[] columns={Constants.ROW_ID,Constants.DATE,Constants.LAUNCH,Constants.DINNER};

        try {
            db = helper.getWritableDatabase();
            Cursor c = db.query(Constants.TB_NAME,columns,null,null,null,null,null);

            Spacecraft s;

            if (c!= null)
            {
                while (c.moveToNext())
                {
                    String s_date=c.getString(1);
                    String s_launch=c.getString(2);
                    String s_dinner=c.getString(3);

                    s=new Spacecraft();
                    s.setDate(s_date);
                    s.setLaunch(s_launch);
                    s.setDinner(s_dinner);

                    spacecrafts.add(s);
                }

            }
        }catch (SQLException e)
        {
            e.printStackTrace();
        }

        return spacecrafts;

    }
}
