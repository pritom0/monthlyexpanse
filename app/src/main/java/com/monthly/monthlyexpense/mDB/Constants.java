package com.monthly.monthlyexpense.mDB;

public class Constants {

    static  final  String ROW_ID="id";
    static final String DATE="date";
    static final String LAUNCH="launch";
    static final String DINNER= "dinner";


    static final String DB_NAME="tv_DB";
    static final String TB_NAME="tv_TB";
    static final int DB_VERSION=1;

    static final String CREATE_TB="CREATE TABLE tv_TB(id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "date TEXT NOT NULL,launch TEXT NOT NULL,dinner TEXT NOT NULL);";


    static final String DROP_TB="DROP TABLE IF EXISTS "+TB_NAME;
}
